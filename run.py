
import pyjapc

import pyjapc_monitoring_adapter
import pyjapc_monitoring_adapter.adapter_core

core = pyjapc_monitoring_adapter.adapter_core.from_config(pyjapc.PyJapc)

core.loop()
