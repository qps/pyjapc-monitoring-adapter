

class ParameterGroup: # pylint: disable=too-few-public-methods,too-many-arguments
    """Wrapper class to describe a single JAPC parameter group.

    Details:
        This class is mostly a internal data storage format, meant to 
        hold necessary information about how to read JAPC parameter
        groups.

        It is mainly automatically generated by other code, e.g.
        the DeviceGroup.load_devices()

    """

    
    def __init__(self, device_group, parameter, 
            interval=1,
            extract_array_index = None,
            table = None):

        self.expanded_parameters = []

        self.parameter = parameter
        self.device_group = device_group
        
        self.expanded_devices = device_group.device_list.keys()

        self.interval = interval
        self.extract_array_index = extract_array_index

        self.table = table

        self.has_been_initialized = False

        for device in self.expanded_devices:
            self.expanded_parameters.append(device + '/' + parameter)
        
