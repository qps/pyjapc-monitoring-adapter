
from functools import wraps
import math
import time
import json

import yaml

import psycopg2

from . import device_group

def pg_arr_to_str(array):
    """Convert an array to compatible PostgreSQL string

    Details:
        This function will attempt to take an array/list, and 
        will format it in a way compatible with PostgreSQL 
        array syntax (which requires curly braces).

    Returns:
        str: a Postgres style string of the array
    """
    str_array = [str(element) for element in array]

    return '{' + ','.join(str_array) + '}'

def pgretry(pg_function):
    """Postgres auto-retry decorator.

    Details:
        This decorator will capture the psycopg2 connection errors, and
        will gracefully try to reconnect and then retry the function.
    
    Note:
        This may retry the function in a try-catch block. Make sure the function
        does not mutate other state, else the retrying might clobber that 
        state further!
    """
    @wraps(pg_function)
    def wrapper(*args, **kw):  # pylint: disable=inconsistent-return-statements
        cls = args[0]
        # pylint: disable=protected-access
        for _ in range(cls._pg_reconnect_tries):
            try:
                return pg_function(*args, **kw)
            except (psycopg2.InterfaceError, psycopg2.OperationalError):
                print ("\nDatabase Connection [InterfaceError or OperationalError]")
                print (f"Idle for {cls._reconnect_idle} seconds")
                time.sleep(cls._reconnect_idle)
                cls._pg_connect()
    return wrapper


def from_config(japc_class, config = 'config.yml', credentials = 'credentials.yml'):
    """Create a functional JAPC Adapter from a config file.

    Details:
        This function provides a convenience method to load a config and create a
        functional JAPC Adapter Core based on either a config and credentials file,
        or by passing a dict of the config/credentials directly.

        Note that you must also pass the JAPC Class to this function. This is
        to avoid a direct dependency on the JAPC libraries, to ease testing.
        If you know of a better way to do this, make a PR!

        The credentials file should include the following:
        > japc:
        >    user: username
        >    password: password
        > db:
        >    >> All options to log in to the Database (e.g. host, port, database, user)

        The config file should include the following:
        > japc:
        >    selector: ""
        >    incaAcceleratorName: LHC
        >    timeZone: utc
        > device_groups:
        >   - list of device groups here

        For the device group options, see DeviceGroup.load_devices()
    """
    if isinstance(config, str):
        with open(config, encoding='utf8') as config_file:
            config = yaml.safe_load(config_file)
    
    if isinstance(credentials, str):
        with open(credentials, encoding='utf8') as credential_file:
            credentials = yaml.safe_load(credential_file)

    japc = None

    if not japc_class is None: 
        japc = japc_class(noSet=True,logLevel=None, **config['japc'])
        japc.rbacLogin(credentials['japc']['user'], credentials['japc']['password'])

    new_core = Core(japc, credentials['db'])

    for device_config in config.get('device_groups', []):
        grp = new_core.add_device_group()
        grp.load_devices(device_config)

    return new_core

class Core:
    """JAPC Adapter core class

    Details:
        This class handles most common values and operations needed
        for the JAPC to PostgreSQL interface.
        
        It is created and used by the user to configure the interface,
        set up which devices and parameters to poll, and to start the
        main loop.

        Internally it is also responsible for the database connection
        to PostgreSQL/TimescaleDB as well as JAPC.
    """
    def __init__(self, japc, pg_params):

        #! psycopg2 connection, reconnects handled internally
        self.pg = None # pylint: disable=invalid-name
        #! dict of user parameters 
        self.pg_conn_params = pg_params

        self._pg_reconnect_idle = 1
        self._pg_reconnect_tries = 5
        
        self.japc = japc

        self.device_groups = []

        self._pg_connect()

    def _pg_connect(self):
        try:
            self.pg.close()
        # Ignoring *all* exceptions because we really don't care
        # if the disconnect worked or is needed.
        except: # pylint: disable=bare-except
            pass

        self.pg = psycopg2.connect(**self.pg_conn_params)


    def japc_get_parameter_group(self, group):
        """Thin wrapper function around JAPC calls.

        Details:
            This function is mainly to provide an easy 
            API / Access point for testing, later overriding, etc.

            It shall pass the call along directly to JAPC
        """
        if self.japc is None:
            print("Returning dummy data!")
            return [ 0 for _ in group ]

        japc_data = self.japc.getParam(group)

        return japc_data

    @pgretry
    def _poll_group(self, parameter_group):
        """Poll a JAPC Group

        This function will handle the entire transaction needed to read
        a given JAPC Group *and* save it to the database.

        The function will first attempt to read data from JAPC (potentially
        warning the user about long function calls for first-time groups).

        The data is then slightly preprocessed (e.g. extracting elements from
        an arra), before being sent to the appropriate TimescaleDB hypertable.

        """
        parameter_string = parameter_group.parameter
        table = parameter_group.table
        if table is None:
            print(f"Using default table japc_data for Parameter {parameter_string}")
            print("Consider setting up a hypertable for it!")

            parameter_group.table = 'japc_data'
            table = 'japc_data'

        param_count = len(parameter_group.expanded_parameters)
        if((not parameter_group.has_been_initialized)
            and (param_count > 10)):
            print(f"Doing a first pull for parameter {parameter_string}.")
            print(f"With {param_count} entries, this will take roughly {param_count/100} minutes!")

        parameter_group.has_been_initialized = True

        group_data = self.japc_get_parameter_group(parameter_group.expanded_parameters)
        
        if not parameter_group.extract_array_index is None:
            group_data = [param_array[parameter_group.extract_array_index] 
                for param_array in group_data]
            parameter_string += f'[{parameter_group.extract_array_index}]'

        with self.pg:
            with self.pg.cursor() as cursor:
                cursor.execute(f"""
                    INSERT INTO {table}
                        (time, device, parameter, value)
                    VALUES
                        ( time_bucket(INTERVAL '1s', NOW()), unnest(%s::text[]), %s, unnest(%s::double precision[]))
                """,
                [pg_arr_to_str(parameter_group.expanded_devices),
                 parameter_string,
                 pg_arr_to_str(group_data)])

    @pgretry
    def _register_device(self, device, metadata = None):
        if metadata is None:
            metadata = {}

        if not isinstance(metadata, dict):
            raise ValueError("Metadata must be a dict!")
        if not isinstance(device, str):
            raise ValueError("Device key must be a string!")
        
        with self.pg:
            with self.pg.cursor() as cursor:
                cursor.execute("""
                    INSERT INTO japc_devices AS d
                        ( device, metadata )
                    VALUES
                        ( %s::text, %s::jsonb )
                    ON CONFLICT (device)
                    DO UPDATE SET metadata = d.metadata || EXCLUDED.metadata
                """,
                [device, json.dumps(metadata)])

    def add_device_group(self):
        """Add a new device group to the adapter core.
         
        Details:
            This function will conveniently create and register a new
            DeviceGroup(), and will return it to the user.

            The user must them configure the device group by setting up
            device names, metadata and which parameters to poll.
            This is most conveniently done via DeviceGroup.load_devices(),
            which can load configuration from a human-readable config file.

        Note:
            It is recommended to not call this function directly. Instead,
            use a config file, and use adapter_core.from_config(), which
            will parse the file and load it for you.
        """
        new_group = device_group.DeviceGroup()
        self.device_groups.append(new_group)

        return new_group


    def _run_tick(self, tick_n):
        tstart = time.time()

        planned_groups = []

        for group in self.device_groups:
            planned_groups += group.get_parameters_this_tick(tick_n)

        for parameter_group in planned_groups:
            self._poll_group(parameter_group)

        print(f"Tick time was: {time.time() - tstart}")

    def loop(self):
        """Set up and enter the polling loop.

        Details:
            This function will set up all devices, register
            them to the database, and will enter the polling loop.

            This function will not return unless there was an error!

            Please make sure all devices have been set up properly before
            calling this function.

            This function is usually called after setting up an adapter
            via adapter_core.from_config().
        """
        print("Performing device registration...")
        
        for group in self.device_groups:
            for device, data in group.device_list.items():
                self._register_device(device, data)

        print("Entering polling loop!")

        while True:
            # This offset ensures clean rounding if the sleep is a bit short
            time.sleep(1 - (time.time() - 0.005) % 1)

            tick_n = math.floor(time.time())
            self._run_tick(tick_n)
