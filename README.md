# pyjapc-polling-adapter

This repository holds the pyJAPC monitoring and polling package.
This package
is intended to easily pull data from a list of CMW/FESA devices using a polling fashion, using the [pyJAPC package](https://gitlab.cern.ch/scripting-tools/pyjapc), and to write this data into a TimescaleDB database for later processing, plotting, and monitoring.

This README will give a rough overview of the installation and operation of one of such scripts.

## Looking for an owner!

This project was made by David Bailey during his temporary internship. Although it is functional, it would be nice to have someone there to maintain and understand it.

All I ask is that you treat it carefully, and use proper pull-requests to merge code, honouring the CI pipelines. We do clean code here.

## Doxygen docs
Automatically generated docs on `release/` tags [are available here](https://pyjapc-monitoring-adapter.docs.cern.ch/)

## Table of contents
1. [Config File Reference](#config-file-quick-reference) to describe the config file options
2. [Installation](#installation)
    - [Getting a Technical Network VM](#pyjapc-virtual-machine)
    - [venv setup](#venv-setup)
    - [SystemD setup](#systemd-setup)
3. [Database Setup](#database-setup)
    - [Adding extra tables](#adding-tables)
4. [Grafana Setup](#grafana-setup)

## Config File Quick Reference

File `config.yml`:
```yml
japc:
    selector: ''
    incaAcceleratorName: LHC
    timeZone: utc
device_groups:  # List of objects. Each object describes a set of devices
                # to monitor, together with their parameters
    - metadata:                     # Metadata is description data added to the database.
        device_type: test_device_a  # It does not affect the acquisition, but is very useful
        location: sector_nth        # in e.g. Grafana plots to sort and query, so set this
                                    # gratuitously!

      prefix: device_prefix.    # Device prefix and postfix will be appended
      postfix: .device_postfix  # or prepended to any device name defined later on.

      load_csv: filename.csv    # Load a CSV filename. The first row MUST be column names.
                                # One colum must be named `device`, other columns are added
                                # to the metadata field.
      exclude:
        - DQAMCNMB.MB.A13R3     # Device names to remove from the list. Useful to adjust
        - DQAMCNMB.MB.A8R4      # device lists after a CSV import
    
      devices:                  # Manually specify a list of devices
        - device_1              # Prefix and Postfix will be added, so these will become:
        - device_2              # device_prefix.device_1.device_postfix
    
      devices: ["python", "style", "string-array", "also-ok"]
      
                # Subgroups allow you to specify "nested" groups of devices. Each subgroup 
                # inherits the metadata, prefix and postfix of the parent, but can add
                # its own metadata and extra prefix/postfix data.
                #
                # This makes it very easy to define large sets of devices that all share
                # e.g. the device type parameter, but have smaller subgroups that want extra
                # metadata
      subgroups:    
        - metadata: {"sector_location": "test-rack-b"}
          devices: ["test_device_a"]
      
      # Finally, define which JAPC parameters shall be extracted. The parameter 
      # name will be joined to the device name with a slash (`/`), and for each
      # defined parameter a separate JAPC get call will be used, for the entire 
      # device group at once.
      parameters:   # "Parameters" needs to be an object
        "Acquisition#values":   # The key here is the name of the parameter to use
            interval: 5             # Interval (in seconds) how often to fetch this value. 
                                    # ONLY INTEGER VALUES ALLOWED (for now!) 
            table: acquisition_data # Which SQL table to insert into

                # *Optional*, when set defines which values to extract
                # from a JAPC Array data element. The resulting parameters will be
                # inserted into the database as `parameter#key[array_index]`
            extract_array_index: 2 
                    

    - example_group_2
    - ...
```

## Installation

### pyJAPC Virtual Machine

Running this code relies on few external dependencies, but the [pyJAPC package](https://gitlab.cern.ch/scripting-tools/pyjapc) is needed!
It is recommended to run this code on a machine that has access to the [AccPy Python environments](https://wikis.cern.ch/display/ACCPY/Getting+started+with+Acc-Py).
AccPy includes almost all needed tools for PyJAPC, especially the Java packages needed for it, so it's the most reliable way of installing it.
The machine must also have access to the Controls-Middleware // FESA.

To solve both of these issues at once, it's easiest to request access to a [Technical Network VM](https://wikis.cern.ch/display/MPESC/Onboarding+checklist). These machines are in the correct network to connect to the API endpoints, and come with AccPy installed.

Once you have a suitable machine chosen, it is *recommended*, but not needed, to clone this repository into `/opt/` on the machine (creating the folder `/opt/pyjapc-monitoring-adapter`).
```bash
cd /opt/
sudo mkdir pyjapc-monitoring-adapter
sudo chown [username] pyjapc-monitoring-adapter
cd pyjapc-monitoring-adapter
git clone https://gitlab.cern.ch/qps/pyjapc-monitoring-adapter.git .
```

If you choose a different folder for your work, make sure to adjust following command paths, if needed.

#### VEnv Setup

We'll `cd` into the folder with our git repository, and create a virtual environment for the code using AccPy. We can also directly install `psycopg2`, as the code needs it.

```bash
source /acc/local/share/python/acc-py/base/pro/setup.sh

acc-py venv /opt/pyjapc-monitoring-adapter/venv
source /opt/pyjapc-monitoring-adapter/venv/bin/activate.sh

python -m pip install pyjapc

sudo yum install libpq # We need LibPQ system libraries!
python -m pip install psycopg2
```

#### SystemD Setup

In order to make this code a proper system service, which will be monitored by systemd, 
restarted upon failures, and can has logs available via `systemctl` and `journalctl`, this
repository includes a base systemd service file.

It can be found [here (file `pyjapc_monitoring.service`)](pyjapc_monitoring.service). 

This file must be either copied or sym-linked (`ln -s`) into a Systemd service file directory. Note that the systemd file uses `/opt/pyjapc-monitoring-adapter` as folder, adjust it if needed! 

```bash
sudo ln -s /opt/pyjapc-monitoring-adapter/pyjapc_monitoring.service /usr/lib/systemd/system/

sudo systemctl daemon-reload
sudo systemctl status pyjapc_monitoring.service
```

This should list the PyJAPC service file as loaded but inactive.

You can now set up your [credentials login as well as JAPC configuration](#config-file-setup).

Make sure you have a [properly configured database](#database-setup), too.

After your configuration is set up, you can enable and start the monitoring service as follows:

```bash
sudo systemctl enable pyjapc_monitoring.service
sudo systemctl start  pyjapc_monitoring.service
```

#### Config file setup

The PyJAPC Code needs two different files to operate:
- `config.yml`:
    This will describe which devices to monitor, which parameters to pull and how often etc.
    Safe to put into a version control
- `credentials.yml`:
    This file contains the RBAC and Database login tokens. Keep it safe!

The config file is described in [the config file quick reference](#config-file-quick-reference).

The credentials file is much smaller, and looks like:
```yml
db:
    host: your-db-addr.cern.ch
    user: your-db-name
    password: your-db-password
    port: your-db-port
japc:
    user: rbac-login-user
    password: rbac-login-password
```

Please note that it might be nice to tie this into the linux credential services. If someone can tweak this, please do!

## Database Setup

This code uses a simple [TimescaleDB](https://www.timescale.com) database as backend. You will need to set it up a little before being able to use this script.

### Initial setup

For users at CERN, this can be done by following the [DBOD setup guide for PostgreSQL](https://dbod-user-guide.web.cern.ch/getting_started/PostgreSQL/postgresql/), then going into the DBOD control panel, "File Editor", and edit `postgresql.conf`:
```
shared_preload_libraries = 'timescaledb'
```

You will also need to tweak the `pg_hba.conf` file to allow connection of your users. In the DBOD panel, File Editor again, `pg_hba.conf`.
Make sure your own database user can get access, e.g.:
`host   all     admin   0.0.0.0/0   md5`


For other users, TimescaleDB docker images are available, or just follow whatever setup guide on their website is most convenient.

Then, to set up a minimal database:
- Connect to the DB with your admin credentials.
- Create a new user+database for the pyJAPC Monitoring
- If needed, create a Grafana read-only user to plot data with.

```SQL
CREATE USER [username] PASSWORD [password];
CREATE DATABASE [db_name] WITH OWNER [username];

\c [db_name]

CREATE USER [grafana-username] PASSWORD [grafana-password];
GRANT USAGE ON SCHEMA public TO [grafana-username];
GRANT SELECT ON ALL TABLES IN SCHEMA public TO [grafana-username];

ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO [grafana-username];

CREATE EXTENSION timescaledb;
```

You should see the confirmation message of Timescale:
```
[db_name]=# CREATE EXTENSION timescaledb; 
WARNING:  
WELCOME TO
 _____ _                               _     ____________  
|_   _(_)                             | |    |  _  \ ___ \ 
  | |  _ _ __ ___   ___  ___  ___ __ _| | ___| | | | |_/ / 
  | | | |  _ ` _ \ / _ \/ __|/ __/ _` | |/ _ \ | | | ___ \ 
  | | | | | | | | |  __/\__ \ (_| (_| | |  __/ |/ /| |_/ /
  |_| |_|_| |_| |_|\___||___/\___\__,_|_|\___|___/ \____/
```

If you do not see this header, make sure the Timescale libraries are loaded in the `shared_preload_libraries` section!

You now need to go back to the `pg_hba.conf` file to add the JAPC and Grafana users. Add the lines:
```
host  [db_name]  [japc_user]    [ip_of_pyjapc_vm] md5
host  [db_name]  [grafana-user] 0.0.0.0/0   md5 
```

Check if you can connect to them via `psql` now, after the database was reloaded.

Now, we also need to add an initial table for JAPC devices and data.
Log in to the database with the JAPC user, then
execute the following commands:

```SQL 
CREATE TABLE japc_devices (
    device TEXT PRIMARY KEY NOT NULL,
    metadata JSONB DEFAULT '{}'
);

CREATE INDEX ON japc_devices(metadata);
```

Now, create a default data table, `japc_data`.
These values are rough suggestions, and can be adjusted as needed. Consult the TimescaleDB documentations for the different functions to see what exactly the values do.

```SQL
CREATE TABLE japc_data (
    time TIMESTAMPTZ NOT NULL,
    device TEXT NOT NULL,
    parameter TEXT NOT NULL,
    value FLOAT NOT NULL
);

SELECT create_hypertable('japc_data', 'time', chunk_time_interval=>INTERVAL '3h');

CREATE INDEX ON japc_data (parameter, device);

ALTER TABLE japc_data 
    SET (timescaledb.compress, 
         timescaledb.compress_segmentby='device,parameter',
         timescaledb.compress_chunk_time_interval='1d');

SELECT add_compression_policy('japc_data', INTERVAL '1d');
SELECT add_retention_policy('japc_data', INTERVAL '1 month');
```

### Adding tables

Each parameter needs to be saved somewhere. This happens in data hypertables.
A general recommendation is to create one table per specific set of data you want to record - e.g. one per device type or even one per parameter.

It makes sense to keep similar parameters in the same table if you want to perform calculations across them, but keeping values separated also simplifies writing queries for them and improves query performance.

Follow the steps above for creating the `japc_data` default table, but swap out the table name and whatever other parameter you want to adjust.

Parameter Groups allow you to specify which table to write into by using the `table` key in the `parameters` section of your config YAML, e.g.:

```yml
    parameters:
        Data#temperatureCalc:
            interval: 5
            table: temperature_data
```

Another thing to consider to boost performance, and allow for the storage of older, less precise data without using too much more memory, is to use [Continuous Aggregates](https://docs.timescale.com/api/latest/continuous-aggregates). These perform certain aggregation operations (averaging, percentiles, etc.) on your data, and store this result separately from the main data table. This way, you can e.g. persistently store 1h averages of your temperature sensors, while the raw 5s readings can be removed after 2 weeks.

## Grafana Setup

[Grafana](https://grafana.com/) is the recommended observability platform to use alongside this adapter to plot and monitor data. CERN already has some instances appropriately set up, the recommended one being [the MONIT instance](https://monit-grafana.cern.ch/). Your section should be able to request access to it and receive its own organization within it.

You can easily add the DBOD PostgreSQL instance as standard Postgres+Timescale instance data source in Grafana. Use the username+password combination supplied during [the database setup](#database-setup) - using the readonly Grafana user, NOT the JAPC-user!

From here, it is recommended to use the [getting started guides](https://grafana.com/docs/grafana/latest/getting-started/build-first-dashboard/) to plot your data.
It's good practice to use the Timescale `time_bucket()` or `time_bucket_gapfill()` options when querying. If your queries are slow, see if you can use a [continuous aggregate](https://docs.timescale.com/use-timescale/latest/continuous-aggregates/) to pre-calculate your data and speed things up a bit more.

Also, for plots such as heatmaps or non-timeseries data, the [Plotly.JS Grafana panel](https://github.com/nline/nline-plotlyjs-panel) works great, if you know Plotly and some very simple Javascript. Much better for high-point-count plots (>2k and above), as well as heatmap plots or similar.

## Support

Originally this script was created by David Bailey, who is most likely no longer at CERN (unless you get lucky and I'm back ;) )

His E-Mail is davidbailey.2889@gmail.com

Otherwise, Magnus Christensen had a hand in setting up some Grafana items as well.

## Authors and acknowledgment

Primary Author: David Bailey (davidbailey.2889@gmail.com)

With thanks to: Magnus Christensen for giving Grafana a footing here,
    Reiner Denz for providing help with RBAC/JAPC and use cases, and Jens Steckert for letting me work on this next to his precious uQDS Channels.
    Some personal thanks also go to my friend Neira, for providing experience with database setups and flexible schema layout.

## License
MIT, baby. Take it, use it, enjoy it, make it.